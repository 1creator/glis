RUN DOCKER CONTAINER:
docker run --name glis -v ${PWD}/dist:/usr/share/nginx/html:ro -d -p 8000:80 nginx

INSTALL JS DEPENDENCIES
npm install

REBUILD ASSETS (CSS/JS) PRODUCTION
npm run prod

REBUILD ASSETS (CSS/JS) DEV
npm run watch