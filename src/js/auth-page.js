import Swiper from 'swiper/js/swiper';

const companyForm = document.querySelector('#sign-up-company-form');
const companyTab = document.querySelector('[href="#company"]');
const labForm = document.querySelector('#sign-up-laboratory-form');
const labTab = document.querySelector('[href="#laboratory"]');

companyTab.addEventListener('click', (el) => {
    companyForm.classList.toggle('show', true);
    labForm.classList.toggle('show', false);
    companyTab.classList.toggle('tabs__item_active', true);
    labTab.classList.toggle('tabs__item_active', false);
});

labTab.addEventListener('click', (el) => {
    companyForm.classList.toggle('show', false);
    labForm.classList.toggle('show', true);
    companyTab.classList.toggle('tabs__item_active', false);
    labTab.classList.toggle('tabs__item_active', true);
});

const swiper = new Swiper('.swiper-container', {
    direction: 'vertical',
    allowTouchMove: false,
    pagination: {
        el: '.swiper-pagination',
    },
});

window.toSlide = (event, page) => {
    event.preventDefault();

    const map = {
        'sign-in': 0,
        'sign-up': 1,
        'sign-up-completed': 2,
        'reset-password': 3,
        'reset-password-completed': 4,
    };

    swiper.slideTo(map[page]);

    return false;
}

