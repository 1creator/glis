const dropdowns = document.querySelectorAll('.dropdown');
const pageContent = document.querySelector('.content');

function handleToggleClick(event) {
    event.stopPropagation();
    if (!this.dropdownEl) return;
    this.dropdownEl.dropdown.toggle();
}

function handleCloseClick(event) {
    if (!this.dropdownEl) return;
    this.dropdownEl.dropdown.toggle(false);
}

function handleContentClick(event) {
    if (event.target === this) {
        this.dropdownEl.dropdown.toggle(false);
        this.removeEventListener('click', handleContentClick)
    }
}

function toggleDropdown(val) {
    const isShow = this.classList.toggle('dropdown_show', val !== undefined ? val : undefined);
    if (pageContent) {
        pageContent.classList.toggle('content_dropdown-show', isShow);
    }

    for (const toggle of this.dropdown.toggles) {
        toggle.setAttribute('data-dropdown-state', isShow ? 'show' : 'hidden');
    }

    if (isShow) {
        pageContent.dropdownEl = this;
        pageContent.addEventListener('click', handleContentClick);
    } else {
        pageContent.removeEventListener('click', handleContentClick);
    }
}

for (const dropdownEl of dropdowns) {
    if (!dropdownEl.id) continue;

    const toggles = document.querySelectorAll(`[data-target="#${dropdownEl.id}"], [href="#${dropdownEl.id}"]`);

    dropdownEl.dropdown = {
        toggles: toggles,
        toggle: toggleDropdown.bind(dropdownEl),
    };

    for (const toggle of toggles) {
        toggle.dropdownEl = dropdownEl;
        toggle.addEventListener('click', handleToggleClick);
    }

    const closeEl = dropdownEl.querySelector('[data-action="close"]');
    if (closeEl) {
        closeEl.dropdownEl = dropdownEl;
        closeEl.addEventListener('click', handleCloseClick)
    }
}