const toggles = document.querySelectorAll('[data-layout]');
for (const toggle of toggles) {
    toggle.addEventListener('click', toggleLayout);
}

function toggleLayout(event) {
    const layout = document.querySelector(this.dataset.target);
    for (const toggle of toggles) {
        toggle.classList.toggle('nav-btn_active', toggle.dataset.layout === this.dataset.layout);
    }
    layout.classList.toggle('alt-layout', this.dataset.layout)
}