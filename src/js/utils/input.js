const inputs = document.querySelectorAll('.input input, .input textarea');

function handleInput(event) {
    this.classList.toggle('filled', !!this.value);
}

for (const inputEl of inputs) {
    inputEl.addEventListener('input', handleInput);
}