const modals = document.querySelectorAll('.modal');
const pageContent = document.querySelector('.content');

function handleToggleClick(event) {
    event.stopPropagation();
    if (!this.modalEl) return;
    this.modalEl.modal.toggle();
}

function handleCloseClick(event) {
    if (!this.modalEl) return;
    this.modalEl.modal.toggle(false);
}

function handleContentClick(event) {
    if (event.target === this) {
        this.modalEl.modal.toggle(false);
        this.removeEventListener('click', handleContentClick)
    }
}

function toggleModal(val) {
    const isShow = this.classList.toggle('modal_show', val !== undefined ? val : undefined);
    if (pageContent) {
        pageContent.classList.toggle('content_dropdown-show', isShow);
    }

    for (const toggle of this.modal.toggles) {
        toggle.setAttribute('data-modal-state', isShow ? 'show' : 'hidden');
    }

    if (isShow) {
        pageContent.modalEl = this;
        pageContent.addEventListener('click', handleContentClick);
    } else {
        pageContent.removeEventListener('click', handleContentClick);
    }
}

for (const modalEl of modals) {
    if (!modalEl.id) continue;

    const toggles = document.querySelectorAll(`[data-target="#${modalEl.id}"], [href="#${modalEl.id}"]`);

    modalEl.modal = {
        toggles: toggles,
        toggle: toggleModal.bind(modalEl),
    };

    for (const toggle of toggles) {
        toggle.modalEl = modalEl;
        toggle.addEventListener('click', handleToggleClick);
    }

    const closeEl = modalEl.querySelector('[data-action="close"]');
    if (closeEl) {
        closeEl.modalEl = modalEl;
        closeEl.addEventListener('click', handleCloseClick)
    }
}