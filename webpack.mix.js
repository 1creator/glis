const mix = require('laravel-mix');
require('laravel-mix-polyfill');

mix.disableSuccessNotifications();
mix.options({processCssUrls: false});

mix.options({
    terser: {
        terserOptions: {
            compress: false,
            mangle: false,
            sourceMap: false,
            keep_fnames: true,
            keep_classnames: true,
        },
    }
});

mix.polyfill({
    enabled: true,
    useBuiltIns: "usage",
    targets: {"firefox": "50", "ie": 11}
});

mix.sass('src/sass/app.scss', 'dist/css/app.css');
mix.js('src/js/utils.js', 'dist/js/utils.js');
mix.js('src/js/auth-page.js', 'dist/js/auth-page.js');
